﻿<?php
/**
 * 帝国7.5接口 Utf-8
 *★★★★★★★★★★★★★★使用说明★★★★★★★★★★★★★★★★
 *★1、放到帝国后台目录下，随便改名字，比如改为 biubiubiu.php
 *★2、找到这行: $password = '这里设置接口密码'; 设置接口密码
 *★3、接口地址为 http://www.xxxxx.com/e/admin/biubiubiu.php
 *★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
 */
$password = '这里设置接口密码';
$postdata = $_POST ?? [];
$postdata['password'] = isset($postdata['password']) ? $postdata['password'] : "";
$postdata['title'] = isset($postdata['title']) ? $postdata['title'] : "";
$postdata['keyword'] = isset($postdata['keyword']) ? $postdata['keyword'] : "";
$postdata['description'] = isset($postdata['description']) ? $postdata['description'] : "";
$postdata['content'] = isset($postdata['content']) ? $postdata['content'] : "";
$postdata['head_pic'] = isset($postdata['head_pic']) ? $postdata['head_pic'] : "";
$postdata['imgs'] = isset($postdata['imgs']) ? $postdata['imgs'] : "";
$postdata['category'] = isset($postdata['category']) ? $postdata['category'] : "";
$postdata['category_ids'] = isset($postdata['category_ids']) ? $postdata['category_ids'] : "";
$postdata['status'] = isset($postdata['status']) ? $postdata['status'] : 1;
$postdata['channelid'] = isset($postdata['channelid']) ? $postdata['channelid'] : "";
if ($password != $postdata['password']) {
    exit('验证密码错误');
}
$cid = $postdata['category_ids'];
if (strpos($cid, ',') !== FALSE) {
    $cids = explode(',', $cid);
    $cid = $cids[mt_rand(0, count($cids) - 1)];
    echo "随机分类CID:" . $cid . "\n";
}
define('EmpireCMSAdmin', '1');
require("../class/connect.php");
require("../class/db_sql.php");
require("../class/functions.php");
require LoadLang("pub/fun.php");
require("../class/delpath.php");
require("../class/copypath.php");
require("../class/t_functions.php");
require("../data/dbcache/class.php");
require("../data/dbcache/MemberLevel.php");
$link = db_connect();
$empire = new mysqlquery();
$enews = 'AddNews';
//
$postdata['classid'] = $cid;
$postdata['bclassid'] = 1;
$postdata['id'] = 0;
$postdata['newstime'] = date("Y-m-d H:i:s", time());
$postdata['banben'] = mt_rand(0, 9) . "." . mt_rand(0, 9);
$postdata['daxiao'] = mt_rand(7, 200) . "MB";
$postdata['yuyan'] = "简体中文";
$postdata['pingfen'] = mt_rand(8, 9) . "." . mt_rand(0, 9);
$postdata['wangluo'] = 2;
$postdata['zhuangtai'] = '运营';
$postdata['checked'] = 1;
//igreenvis
$postdata['yxpf'] = mt_rand(8, 9) . "." . mt_rand(0, 9);
$postdata['yxyy'] = '简体中文';
$postdata['yxdx'] = mt_rand(7, 200) . "MB";
$postdata['yxbb'] = mt_rand(0, 9) . "." . mt_rand(0, 9);
$postdata['yxfl'] = 1;
if (!empty($_GET['yxfl'])) {
    $yxfls = explode(',', $_GET['yxfl']);
    $postdata['yxfl'] = $yxfls[mt_rand(0, count($yxfls) - 1)];
}
//随机游戏栏目
// $gameclass = [];
// foreach($class_r as $c){
// 	if($c['bclassid']==$bid){
// 		$gameclass[] = $c;
// 	}
// }
// $randomclass =  $gameclass[mt_rand(0,count($gameclass)-1)];
//$postdata['classid'] = $randomclass['classid'];
$postdata['classid'] = $cid;
//转换
$postdata['titlepic'] = $postdata['head_pic'];
$postdata['keyboard'] = $postdata['keyword'];
$postdata['newstext'] = $postdata['content'];
$postdata['smalltext'] = $postdata['description'];
//副标题
if (is_file('kw.txt')) {
    $kw = file_get_contents("kw.txt");
    $kws = explode("\n", $kw);
    $randomkw = $kws[mt_rand(0, count($kws) - 1)];
    $postdata['ftitle'] = $postdata['title'] . "_" . $postdata['title'] . $randomkw;
}
//print_r($postdata);die();
$postdata['pics'] = str_replace('-internal.', '.', $postdata['imgs']);
$postdata['titlepic'] = str_replace('-internal.', '.', $postdata['titlepic']);
$picfolder = '/d/file/p/' . date('Y', time()) . '/' . date('m-d', time()) . '/';
if (!is_dir('../..' . $picfolder)) {
    echo "make pic folder";
    mkdir('../..' . $picfolder, 777, TRUE);
    echo "make pic folder ok";
}
$dm = '//' . $_SERVER['SERVER_NAME'];
$pics = explode(',', $postdata['pics']);

$body = $postdata['newstext'];
foreach ($pics as $p) {
    $file = pathinfo($p);
    $filename = $file['basename'];
    $img = file_get_contents($p);
    $localpath = $picfolder . $filename;
    $file_in =  file_put_contents('../..' . $localpath, $img);
    $error = error_get_last();
    $postdata['msmallpic'][] = $dm . $localpath;
    $postdata['mbigpic'][] = $dm . $localpath;
    $imgurls .= "<p><img src='" . "//" . $_SERVER['SERVER_NAME'] . "/$localpath'/></p>\r\n";

}
$body = $body . "\n<br />\n" . $imgurls;
$postdata['newstext'] = $body;
//图标
$file = pathinfo($postdata['titlepic']);
$filename = $file['basename'];
$img = file_get_contents($postdata['titlepic']);
$localpath = $picfolder . $filename;
file_put_contents('../..' . $localpath, $img);
$postdata['titlepic'] = $dm . $localpath;
// //验证用户
// $lur=is_login();
$logininid = 1;//$lur['userid'];
$loginin = 'delong_1';//$lur['username'];
// $loginrnd=$lur['rnd'];
// $loginlevel=$lur['groupid'];
// $loginadminstyleid=$lur['adminstyleid'];
//spurl
if ($enews == 'AddNews' || $enews == 'EditNews' || $enews == 'EditInfoSimple' || $enews == 'AddInfoToReHtml' || $enews == 'DoInfoAndSendNotice' || $enews == 'CheckNews_all' || $enews == 'NoCheckNews_all') {
    hSetSpFromUrl();
}
//hCheckEcmsRHash();
@set_time_limit(0);
$incftp = 0;
if ($public_r['phpmode']) {
    include("../class/ftp.php");
    $incftp = 1;
}
//防采集
if ($public_r['opennotcj']) {
    //@include("../data/dbcache/notcj.php");
}
//会员
require("../member/class/user.php");
require("../class/hinfofun.php");
if ($enews == "AddNews")//增加信息
{
    $navtheid = (int)$postdata['filepass'];
    AddNews($postdata, $logininid, $loginin);
} elseif ($enews == "EditNews")//修改信息
{
    $navtheid = (int)$postdata['id'];
    EditNews($postdata, $logininid, $loginin);
} elseif ($enews == "EditInfoSimple")//修改信息(快速)
{
    $navtheid = (int)$postdata['id'];
    EditInfoSimple($postdata, $logininid, $loginin);
} elseif ($enews == "DelNews")//删除信息
{
    $id = $_GET['id'];
    $classid = $_GET['classid'];
    $bclassid = $_GET['bclassid'];
    DelNews($id, $classid, $logininid, $loginin);
} elseif ($enews == "DelNews_all")//批量删除信息
{
    $id = $postdata['id'];
    $classid = $postdata['classid'];
    $bclassid = $postdata['bclassid'];
    $ecms = $postdata['ecmscheck'] ? 2 : 0;
    DelNews_all($id, $classid, $logininid, $loginin, $ecms);
} elseif ($enews == "EditMoreInfoTime")//批量修改信息时间
{
    EditMoreInfoTime($postdata, $logininid, $loginin);
} elseif ($enews == "DelInfoDoc_all")//删除归档
{
    $id = $postdata['id'];
    $classid = $postdata['classid'];
    $bclassid = $postdata['bclassid'];
    DelNews_all($id, $classid, $logininid, $loginin, 1);
} elseif ($enews == 'AddInfoToReHtml')//刷新页面
{
    AddInfoToReHtml($_GET['classid'], $_GET['dore']);
} elseif ($enews == "TopNews_all")//信息置顶
{
    $bclassid = $postdata['bclassid'];
    $classid = $postdata['classid'];
    $id = $postdata['id'];
    $istop = $postdata['istop'];
    TopNews_all($classid, $id, $istop, $logininid, $loginin);
} elseif ($enews == "CheckNews_all")//审核信息
{
    $bclassid = $postdata['bclassid'];
    $classid = $postdata['classid'];
    $id = $postdata['id'];
    CheckNews_all($classid, $id, $logininid, $loginin);
} elseif ($enews == "NoCheckNews_all")//取消审核信息
{
    $bclassid = $postdata['bclassid'];
    $classid = $postdata['classid'];
    $id = $postdata['id'];
    NoCheckNews_all($classid, $id, $logininid, $loginin);
} elseif ($enews == "MoveNews_all")//移动信息
{
    $bclassid = $postdata['bclassid'];
    $classid = $postdata['classid'];
    $id = $postdata['id'];
    $to_classid = $postdata['to_classid'];
    MoveNews_all($classid, $id, $to_classid, $logininid, $loginin);
} elseif ($enews == "CopyNews_all")//复制信息
{
    $bclassid = $postdata['bclassid'];
    $classid = $postdata['classid'];
    $id = $postdata['id'];
    $to_classid = $postdata['to_classid'];
    CopyNews_all($classid, $id, $to_classid, $logininid, $loginin);
} elseif ($enews == "MoveClassNews")//批量移动信息
{
    $add = $postdata['add'];
    MoveClassNews($add, $logininid, $loginin);
} elseif ($enews == "GoodInfo_all")//批量推荐/头条信息
{
    $classid = $postdata['classid'];
    $id = $postdata['id'];
    $doing = $postdata['doing'];
    $isgood = empty($doing) ? $postdata['isgood'] : $postdata['firsttitle'];
    GoodInfo_all($classid, $id, $isgood, $doing, $logininid, $loginin);
} elseif ($enews == "SetAllCheckInfo")//本栏目信息全部审核
{
    $classid = $_GET['classid'];
    $bclassid = $_GET['bclassid'];
    SetAllCheckInfo($bclassid, $classid, $logininid, $loginin);
} elseif ($enews == "DoWfInfo")//签发信息
{
    DoWfInfo($postdata, $logininid, $loginin);
} elseif ($enews == "DelInfoData")//删除信息页面
{
    $start = $_GET['start'];
    $classid = $_GET['classid'];
    $from = $_GET['from'];
    $retype = $_GET['retype'];
    $startday = $_GET['startday'];
    $endday = $_GET['endday'];
    $startid = $_GET['startid'];
    $endid = $_GET['endid'];
    $tbname = $_GET['tbname'];
    DelInfoData($start, $classid, $from, $retype, $startday, $endday, $startid, $endid, $tbname, $_GET, $logininid, $loginin);
} elseif ($enews == "InfoToDoc")//归档信息
{
    if ($_GET['ecmsdoc'] == 1)//栏目
    {
        InfoToDoc_class($_GET, $logininid, $loginin);
    } elseif ($_GET['ecmsdoc'] == 2)//条件
    {
        InfoToDoc($_GET, $logininid, $loginin);
    } else//信息
    {
        InfoToDoc_info($postdata, $logininid, $loginin);
    }
} elseif ($enews == "DoInfoAndSendNotice")//处理信息并通知
{
    $doing = (int)$postdata['doing'];
    $adddatar = $postdata;
    if ($doing == 1)//删除
    {
        $enews = 'DelNews';
        DelNews($adddatar['id'], $adddatar['classid'], $logininid, $loginin);
    } elseif ($doing == 2)//审核通过
    {
        $enews = 'CheckNews_all';
        $doid[0] = $adddatar['id'];
        CheckNews_all($adddatar['classid'], $doid, $logininid, $loginin);
    } elseif ($doing == 3)//取消审核
    {
        $enews = 'NoCheckNews_all';
        $doid[0] = $adddatar['id'];
        NoCheckNews_all($adddatar['classid'], $doid, $logininid, $loginin);
    } elseif ($doing == 4)//转移
    {
        $enews = 'MoveNews_all';
        $doid[0] = $adddatar['id'];
        MoveNews_all($adddatar['classid'], $doid, $adddatar['to_classid'], $logininid, $loginin);
    }
} else {

    //printerror("ErrorUrl","history.go(-1)");
}
db_close();
$empire = NULL;
?>
